<?php


namespace Divante\Integration\Parser;


class Supplier3Parser implements ParserInterface
{
    public static function getType()
    {
        return 'json';
    }

    public function parse($content)
    {
        $configData = json_decode($content, true);
        if (!isset($configData['list'])) {
            throw new InvalidSourceStructureException("No `list` key found");
        }

        $products = [];
        foreach ($configData['list'] as $key => $value) {
            $products[] = [$value['id'], $value['name'], ''];
        }

        return $products;
    }
}
