<?php


namespace Divante\Integration\Parser;

class Supplier1Parser implements ParserInterface
{
    public static function getType()
    {
        return 'xml';
    }

    public function parse($content)
    {
        $configData = (array)simplexml_load_string($content);
        if (!isset($configData['product'])) {
            throw new InvalidSourceStructureException("No `product` node found");
        }

        $products = [];
        foreach ($configData['product'] as $key => $value) {
            $products[] = [(string)$value->id, (string)$value->name, (string)$value->desc];
        }

        return $products;
    }
}
