<?php


namespace Divante\Integration\Parser;


class Supplier2Parser implements ParserInterface
{
    public static function getType()
    {
        return 'xml';
    }

    public function parse($content)
    {
        $configData = (array)simplexml_load_string($content);
        if (!isset($configData['item'])) {
            throw new InvalidSourceStructureException("No `item` node found");
        }

        $products = [];
        foreach ($configData['item'] as $key => $value) {
            $products[] = [(string)$value->key, (string)$value->title, (string)$value->description];
        }

        return $products;
    }
}
