<?php


namespace Divante\Integration\Parser;


class Factory implements FactoryInterface
{
    /**
     * @var string
     */
    private $namespacePrefix;

    public function __construct($namespacePrefix)
    {
        $this->namespacePrefix = $namespacePrefix;
    }

    /**
     * @param string $type
     * @return ParserInterface
     */
    public function getParser($type)
    {
        $className = $this->namespacePrefix . $type . 'Parser';
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf("Class %s does not exist!", $className));
        }

        return new $className;
    }
}