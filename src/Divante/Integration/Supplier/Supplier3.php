<?php


namespace Divante\Integration\Supplier;


class Supplier3 extends SupplierAbstract
{
    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'Supplier3';
    }

    /**
     * {@inheritdoc}
     */
    public static function getResponseType()
    {
        return 'application/json';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse()
    {
        return $this->parser->parse($this->getResponse());
    }

    /**
     * Simulate get response method
     *
     * @return string
     */
    protected function getResponse()
    {
        return file_get_contents('http://localhost/supplier3.json');
    }
}