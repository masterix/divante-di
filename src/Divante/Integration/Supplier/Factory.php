<?php

/*
 * This file is part of the "Divante/Integration" package.
 *
 * (c) Divante Sp. z o. o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Divante\Integration\Supplier;

use Divante\Integration\IntegrationEvents;
use Divante\Integration\Parser\FactoryInterface as ParserFactoryInterface;
use Divante\Integration\Supplier\Listener\ProductsListener;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class Factory
 *
 * @package Divante\Integration\Supplier
 */
class Factory implements FactoryInterface
{
    /**
     * Parser factory
     *
     * @var ParserFactoryInterface
     */
    protected $parserFactory;

    /**
     * Event dispatcher
     *
     * @var EventDispatcher
     */
    protected $eventDispatcher;

    /**
     * @var string
     */
    protected $namespacePrefix;

    /**
     * Constructor
     *
     * @param ParserFactoryInterface $parserFactory
     * @param EventDispatcher $eventDispatcher
     * @param string $namespacePrefix
     */
    public function __construct(ParserFactoryInterface $parserFactory, EventDispatcher $eventDispatcher, $namespacePrefix)
    {
        $this->parserFactory = $parserFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->namespacePrefix = $namespacePrefix;

        // add listener
        $this->eventDispatcher->addListener(
            IntegrationEvents::SUPPLIER_GET_PRODUCTS,
            [new ProductsListener(), 'logProducts']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getSupplier($supplierName)
    {
        $className = $this->namespacePrefix . $supplierName;
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf("Class %s does not exist!", $supplierName));
        }

        $parser = $this->parserFactory->getParser($supplierName);

        return new $className($parser, $this->eventDispatcher);
    }
}
