<?php


namespace Divante\Integration\Supplier;


class Supplier2 extends SupplierAbstract
{
    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'Supplier2';
    }

    /**
     * {@inheritdoc}
     */
    public static function getResponseType()
    {
        return 'application/xml';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse()
    {
        return $this->parser->parse($this->getResponse());
    }

    /**
     * Simulate get response method
     *
     * @return string
     */
    protected function getResponse()
    {
        return file_get_contents('http://localhost/supplier2.xml');
    }
}