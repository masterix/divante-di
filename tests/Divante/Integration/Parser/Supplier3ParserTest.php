<?php


namespace tests\Divante\Integration\Parser;


use Divante\Integration\Parser\Supplier3Parser;

class Supplier3ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Supplier3Parser
     */
    private $parser;

    public function setUp()
    {
        $this->parser = new Supplier3Parser();
    }

    public function testSupplier1ParserReadsProducts()
    {
        $xmlContent = file_get_contents(__DIR__ . '/validJsonSupplier3.json');
        $products = $this->parser->parse($xmlContent);

        $this->assertEquals(3, count($products));
        $this->assertEquals('999-ABC-DEF-1', $products[0][0]);
        $this->assertEquals('Product 1', $products[0][1]);
        $this->assertEquals('', $products[1][2]);
    }

    /**
     * @expectedException Divante\Integration\Parser\InvalidSourceStructureException
     */
    public function testSupplier1ParserThrowsExceptionWhenInvalidXmlProvided()
    {
        $invalidXmlContent = file_get_contents(__DIR__ . '/invalidJsonSupplier3.json');

        $this->parser->parse($invalidXmlContent);
    }
}
