<?php


namespace tests\Divante\Integration\Parser;

use Divante\Integration\Parser\Factory;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Factory
     */
    private $factory;

    public function setUp()
    {
        $testNamespacePrefix = 'tests\Divante\Integration\Parser\\';
        $this->factory = new Factory($testNamespacePrefix);
    }

    public function testParserFactoryReturnsParserObjectWhenGivenCorrectName()
    {
        $parser = $this->factory->getParser('TestSupplier');

        $this->assertInstanceOf(TestSupplierParser::class, $parser);
    }

    public function testParserFactoryReturnsParserObjectWhenGivenCaseInsensitiveName()
    {
        $parser = $this->factory->getParser('testsupplier');

        $this->assertInstanceOf(TestSupplierParser::class, $parser);
    }

    /**
     * @expectedException \Exception
     */
    public function testParserFactoryThrowsExceptionWhenInvalidSupplierNameProvided()
    {
        $this->factory->getParser('test');
    }
}
