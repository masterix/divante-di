<?php

namespace tests\Divante\Integration\Parser;

use Divante\Integration\Parser\Supplier1Parser;

class Supplier1ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Supplier1Parser
     */
    private $parser;

    public function setUp()
    {
        $this->parser = new Supplier1Parser();
    }

    public function testSupplier1ParserReadsProducts()
    {
        $xmlContent = file_get_contents(__DIR__ . '/validXmlSupplier1.xml');
        $products = $this->parser->parse($xmlContent);

        $this->assertEquals(2, count($products));
        $this->assertEquals('123-456-1', $products[0][0]);
        $this->assertEquals('Product 1', $products[0][1]);
        $this->assertEquals('Product 2 description', $products[1][2]);
    }

    /**
     * @expectedException Divante\Integration\Parser\InvalidSourceStructureException
     */
    public function testSupplier1ParserThrowsExceptionWhenInvalidXmlProvided()
    {
        $invalidXmlContent = file_get_contents(__DIR__ . '/invalidXmlSupplier1.xml');

        $this->parser->parse($invalidXmlContent);
    }
}
