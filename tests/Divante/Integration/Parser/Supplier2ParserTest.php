<?php


namespace tests\Divante\Integration\Parser;

use Divante\Integration\Parser\Supplier2Parser;

class Supplier2ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Supplier2Parser
     */
    private $parser;

    public function setUp()
    {
        $this->parser = new Supplier2Parser();
    }

    public function testSupplier1ParserReadsProducts()
    {
        $xmlContent = file_get_contents(__DIR__ . '/validXmlSupplier2.xml');
        $products = $this->parser->parse($xmlContent);

        $this->assertEquals(2, count($products));
        $this->assertEquals('CC-123-456-1', $products[0][0]);
        $this->assertEquals('Product 1', $products[0][1]);
        $this->assertEquals('Product 2 descriptionription', $products[1][2]);
    }

    /**
     * @expectedException Divante\Integration\Parser\InvalidSourceStructureException
     */
    public function testSupplier2ParserThrowsExceptionWhenInvalidXmlProvided()
    {
        $invalidXmlContent = file_get_contents(__DIR__ . '/invalidXmlSupplier2.xml');

        $this->parser->parse($invalidXmlContent);
    }
}