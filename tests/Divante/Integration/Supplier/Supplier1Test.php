<?php

namespace tests\Divante\Integration\Supplier;

use Divante\Integration\Supplier\Supplier1;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\Divante\Integration\Parser\TestSupplierParser;

class Supplier1Test extends \PHPUnit_Framework_TestCase
{
    public function testSupplier1HasCorrectName()
    {
        $this->assertEquals('Supplier1', Supplier1::getName());
    }

    public function testSupplier1HasCorrectResponseType()
    {
        $this->assertEquals('application/xml', Supplier1::getResponseType());
    }

    public function testSupplier1DispatchesEvent()
    {
        $eventDispatcherMock = $this->getMockBuilder(EventDispatcher::class)
            ->setMethods(['dispatch'])
            ->getMock();

        $eventDispatcherMock
            ->expects($this->once())
            ->method('dispatch');

        $supplier = new Supplier1(new TestSupplierParser(), $eventDispatcherMock);

        $supplier->getProducts();
    }
}
