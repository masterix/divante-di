<?php


namespace tests\Divante\Integration\Supplier;


use Divante\Integration\Parser\ParserInterface;
use Divante\Integration\Supplier\SupplierInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class TestSupplier implements SupplierInterface
{
    public function __construct(ParserInterface $parser, EventDispatcher $eventDispatcher)
    {

    }

    public function getProducts()
    {

    }

    public static function getName()
    {
        return 'test';
    }

    public static function getResponseType()
    {

    }


}