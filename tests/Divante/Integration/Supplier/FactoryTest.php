<?php

namespace tests\Divante\Integration\Supplier;

use Divante\Integration\Supplier\Factory as SupplierFactory;
use Divante\Integration\Parser\Factory as ParserFactory;
use Divante\Integration\Supplier\SupplierInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\Divante\Integration\Parser\TestSupplierParser;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    const TEST_PARSER_NAMESPACE_PREFIX = 'tests\Divante\Integration\Parser\\';

    const TEST_SUPPLIER_NAMESPACE_PREFIX = 'tests\Divante\Integration\Supplier\\';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $parserFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $eventDispatcherMock;

    public function setUp()
    {
        $this->parserFactoryMock = $this->getMockBuilder(ParserFactory::class)
            ->setConstructorArgs([self::TEST_PARSER_NAMESPACE_PREFIX])
            ->getMock();

        $this->eventDispatcherMock = $this->getMockBuilder(EventDispatcher::class)
            ->setMethods(['addListener'])
            ->getMock();
    }

    public function testSupplierFactoryReturnsSupplier()
    {
        $this->parserFactoryMock->expects($this->once())
            ->method('getParser')
            ->willReturn(new TestSupplierParser());

        $this->eventDispatcherMock->expects($this->once())
            ->method('addListener')
            ->withAnyParameters();

        $factory = new SupplierFactory(
            $this->parserFactoryMock,
            $this->eventDispatcherMock,
            self::TEST_SUPPLIER_NAMESPACE_PREFIX
        );

        $supplier = $factory->getSupplier('TestSupplier');

        $this->assertInstanceOf(SupplierInterface::class, $supplier);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSupplierFactoryThrowsExceptionWhenInvalidSupplierNameProvided()
    {
        $factory = new SupplierFactory(
            $this->parserFactoryMock,
            $this->eventDispatcherMock,
            self::TEST_SUPPLIER_NAMESPACE_PREFIX
        );

        $factory->getSupplier('InvalidTestSupplier');
    }

}