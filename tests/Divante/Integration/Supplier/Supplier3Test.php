<?php

namespace tests\Divante\Integration\Supplier;

use Divante\Integration\Supplier\Supplier3;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\Divante\Integration\Parser\TestSupplierParser;

class Supplier3Test extends \PHPUnit_Framework_TestCase
{
    public function testSupplier2HasCorrectName()
    {
        $this->assertEquals('Supplier3', Supplier3::getName());
    }

    public function testSupplier2HasCorrectResponseType()
    {
        $this->assertEquals('application/json', Supplier3::getResponseType());
    }

    public function testSupplier2DispatchesEvent()
    {
        $eventDispatcherMock = $this->getMockBuilder(EventDispatcher::class)
            ->setMethods(['dispatch'])
            ->getMock();

        $eventDispatcherMock
            ->expects($this->once())
            ->method('dispatch');

        $supplier = new Supplier3(new TestSupplierParser(), $eventDispatcherMock);

        $supplier->getProducts();
    }
}
