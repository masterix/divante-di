<?php

namespace tests\Divante\Integration\Supplier;

use Divante\Integration\Supplier\Supplier2;
use Symfony\Component\EventDispatcher\EventDispatcher;
use tests\Divante\Integration\Parser\TestSupplierParser;

class Supplier2Test extends \PHPUnit_Framework_TestCase
{
    public function testSupplier2HasCorrectName()
    {
        $this->assertEquals('Supplier2', Supplier2::getName());
    }

    public function testSupplier2HasCorrectResponseType()
    {
        $this->assertEquals('application/xml', Supplier2::getResponseType());
    }

    public function testSupplier2DispatchesEvent()
    {
        $eventDispatcherMock = $this->getMockBuilder(EventDispatcher::class)
            ->setMethods(['dispatch'])
            ->getMock();

        $eventDispatcherMock
            ->expects($this->once())
            ->method('dispatch');

        $supplier = new Supplier2(new TestSupplierParser(), $eventDispatcherMock);

        $supplier->getProducts();
    }
}
